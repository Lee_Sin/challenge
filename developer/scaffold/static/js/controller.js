app.controller('MainCtrl', function($scope, $http, Customer, $state,$window){
    $scope.editCustomer = {};
    
    $scope.update = function(customer) {
        Customer.update(customer);
        //$scope.myhtml = '<div class="alert alert-success" role="alert"><strong>Well done!</strong> You successfully updated</div>'; 
    };

    $scope.send_customer = function(c, id, $event) {

        $http({
            url: 'http://localhost:8010/api/v1/customerdata/' +c.id,
            method: 'GET'
        }).success(function(data, status, headers, config) {

            $scope.editCustomer.id= data.id;
            $scope.editCustomer.SITE_NAME = data.data.SITE_NAME;
            $scope.editCustomer.LAST_ACCESSED = data.data.LAST_ACCESSED;
            $scope.editCustomer.TIME_ZONE_DISPLAYED_FOR_DEADLINES = data.data.TIME_ZONE_DISPLAYED_FOR_DEADLINES;
            $scope.editCustomer.PLATFORM_NAME = data.data.PLATFORM_NAME;
            $scope.editCustomer.LANGUAGE_CODE = data.data.LANGUAGE_CODE;
            $scope.editCustomer.ENABLE_MKTG_SITE = data.data.ENABLE_MKTG_SITE;
        });
    };


    Customer.all().then(function(res){
        $scope.customers = res.data;
    });

});