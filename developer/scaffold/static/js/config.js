var app = angular.module('test', [
    'ui.router','ngSanitize'
]);

app.constant('BASE_URL', 'http://localhost:8010/api/v1/customerdata/');

app.config(function($stateProvider, $urlRouterProvider){
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: '/static/templates/customer_list.html',
            controller: 'MainCtrl'
        });

    $urlRouterProvider.otherwise('/');
});


app.service('Customer', function($http, BASE_URL, $window){
    var Customer = {};

    Customer.all = function(){
        return $http.get(BASE_URL);
    };

    Customer.update = function(updated){
       // return $http.put(BASE_URL + updated.id +'/', JSON.stringify({'data':updated}));
       $http({
            url: 'http://localhost:8010/api/v1/customerdata/' + updated.id +'/',
            data: JSON.stringify({'data':updated}),
            method: 'PUT'
        }).success(function(data, status, headers, config) {
            //$scope.myhtml = '<div class="alert alert-success" role="alert"><strong>Well done!</strong> You successfully updated</div>';
            if (status=200){
                alert('You successfully updated');
                $window.location.reload();
            }else{
                alert('Data Error');
                $window.location.reload();
            }
            
        });
    };

    return Customer;
});