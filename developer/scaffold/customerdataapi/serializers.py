# -*- coding: utf-8 -*-
"""
Database models for customerdataapi.
"""

from __future__ import absolute_import, unicode_literals

from rest_framework import serializers
from customerdataapi.models import CustomerData


class CustomerDataSerializer(serializers.ModelSerializer):
    """
    A simple serializer for our CustomerData model
    """
    id = serializers.ReadOnlyField()
    data = serializers.JSONField()

    def update(self, instance, validated_data):
    
        id= instance.pk
        required_fields = ['ENABLE_MKTG_SITE','LANGUAGE_CODE','PLATFORM_NAME',
                           'SITE_NAME','TIME_ZONE_DISPLAYED_FOR_DEADLINES','LAST_ACCESSED']
        instance.data = validated_data.get('data', instance.data)
        ins = CustomerData.objects.get(pk=id)

        for k in required_fields :
            if instance.data.has_key(k):
                ins.data[k]= instance.data.get(k)

        ins.save()        
        return ins

    class Meta:
        model = CustomerData
        fields = ('id','data',)