# -*- coding: utf-8 -*-
"""
Views for customerdataapi.
"""
from __future__ import absolute_import, unicode_literals

from rest_framework import viewsets, permissions,generics
from rest_framework.response import Response
from customerdataapi.models import CustomerData
from customerdataapi.serializers import CustomerDataSerializer
import simplejson as json


class CustomerDataViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving CustomerData.
    """
    queryset = CustomerData.objects.all()
    serializer_class = CustomerDataSerializer
    permission_classes = (permissions.AllowAny,)

    '''def get_object(self, pk):
        try:
            return CustomerData.objects.get(pk=pk)
        except CustomerData.DoesNotExist:
            raise Http404'''


    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        print 'query'
        queryset = self.get_queryset()
        serializer = CustomerDataSerializer(queryset, many=True)
        return Response(serializer.data)


    #def update(self, request, pk=None):

    	#import ipdb;ipdb.set_trace();
    	#data = request.data
    	#print 'update',json.dumps(data)
    	#return Response('hola')